package dao;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import pojo.BodyPart;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = { BodyPartDAO.class, BodyPart.class })
@AutoConfigurationPackage
@Transactional
@EntityScan(basePackages = {"pojo"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BodyPartDAOTest {

    private EntityManager em;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private BodyPartDAO bodyPartDAO;

    @Before
    public void setUp() throws Exception {
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.persist(new BodyPart("Biceps"));
        em.persist(new BodyPart("Legs"));
        em.persist(new BodyPart("Back"));
        em.getTransaction().commit();
    }

    @Test
    public void testGetAllBodyParts() {
        List<BodyPart> bodyparts = bodyPartDAO.getAllBodyParts();
        assertEquals(3, bodyparts.size());
    }

    @Test
    public void testGetRequiredBodyPart() {
        String requiredBodyPart = "Back";
        BodyPart retrievedBodyPart = bodyPartDAO.getRequiredBodyPart(requiredBodyPart);
        String retrievedBodyPartName;

        if (retrievedBodyPart != null) {
            retrievedBodyPartName = retrievedBodyPart.getName();
        } else {
            retrievedBodyPartName = "null";
        }
        assertEquals("Back", retrievedBodyPartName);
    }

    @Test
    public void testGetRequiredBodyPartNull() {
        String requiredBodyPart = "Non existent body part";
        BodyPart retrievedBodyPart = bodyPartDAO.getRequiredBodyPart(requiredBodyPart);

        assertNull(retrievedBodyPart);
    }

    @Test
    public void testInsertNewBodyPart() {
        String tricepsString = "Triceps";
        BodyPart triceps = new BodyPart(tricepsString);
        bodyPartDAO.insertBodyPart(triceps);

        String getQuery = "from BodyPart";
        int actualCount = entityManagerFactory.unwrap(SessionFactory.class).openSession().createQuery(getQuery).getResultList().size();
        assertEquals(4, actualCount);
    }

    @After
    public void tearDown() throws Exception {
        em.clear();

    }
}

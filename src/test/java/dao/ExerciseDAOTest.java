package dao;

import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;
import pojo.CardioExercise;
import pojo.GeneralExercise;
import pojo.WeightExercise;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = { ExerciseDAO.class, GeneralExercise.class, CardioExercise.class, WeightExercise.class })
@AutoConfigurationPackage
@Transactional
@EntityScan(basePackages = {"pojo"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ExerciseDAOTest {
    private EntityManager em;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private ExerciseDAO exerciseDAO;

    @Before
    public void setUp() throws Exception {
        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();

        // Create weight exercise
        String name = "Biceps Curls";
        int sets = 3;
        int reps = 8;
        double weight = 12.5;
        GeneralExercise weightExercise = new WeightExercise(name, sets, reps, weight);

        // Create cardio exercise
        String cardioName = "Running";
        double amountOfTime = 75;
        GeneralExercise cardioExercise = new CardioExercise(cardioName, amountOfTime);

        em.persist(weightExercise);
        em.persist(cardioExercise);

        em.getTransaction().commit();
    }

    @Test
    public void testInsertNewWeightExercise() {
        GeneralExercise newWeightExercise = new WeightExercise("Bench Press", 3, 8 , 100);
        GeneralExercise newWeightExercise2 = new WeightExercise("Squats", 3, 8 , 150);
        exerciseDAO.insertNewExercise(newWeightExercise);
        exerciseDAO.insertNewExercise(newWeightExercise2);

        String getQuery = "from WeightExercise";
        int actualCount = entityManagerFactory.unwrap(SessionFactory.class).openSession().createQuery(getQuery).getResultList().size();

        assertEquals(3, actualCount);
    }

    @Test
    public void testInsertNewCardioExercise() {
        GeneralExercise newCardioExercise = new CardioExercise("Cycling", 60);
        exerciseDAO.insertNewExercise(newCardioExercise);

        String getQuery = "from CardioExercise";
        int actualCount = entityManagerFactory.unwrap(SessionFactory.class).openSession().createQuery(getQuery).getResultList().size();

        assertEquals(2, actualCount);
    }
}

package dao;

import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import pojo.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = { WorkoutExerciseDAO.class, WorkoutExercise.class })
@AutoConfigurationPackage
@Transactional
@EntityScan(basePackages = {"pojo"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class WorkoutExerciseDAOTest {

    private EntityManager em;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private WorkoutExerciseDAO workoutExerciseDAO;

    private GeneralExercise cardioExercise;
    private GeneralExercise weightExercise;

    private WorkoutExercise workoutExerciseCardio;
    private WorkoutExercise workoutExerciseWeight;

    @Before
    public void setUp() throws Exception {
        em = entityManagerFactory.createEntityManager();

        cardioExercise = new CardioExercise("Rowing", 50.5);
        weightExercise = new WeightExercise("Dips", 4, 10, 10);
        workoutExerciseCardio = new WorkoutExercise(cardioExercise, new Date(), "default");
        workoutExerciseWeight = new WorkoutExercise(weightExercise, new Date(), "default");

        em.getTransaction().begin();
        em.persist(workoutExerciseCardio);
        em.persist(workoutExerciseWeight);

        em.getTransaction().commit();
    }

    @Test
    public void testGetAllRecordedWorkouts() {
        int count = workoutExerciseDAO.getAllRecordedWorkouts().size();
        assertEquals(2, count);
    }

    @Test
    public void testGetAllRecordedWorkoutsForExercise() {
        int count = workoutExerciseDAO.getAllRecordedWorkoutsWithExercise(weightExercise).size();
        assertEquals(1, count);
    }

    @Test
    public void testInsertNewExercise() {
        GeneralExercise newExercise = new WeightExercise("Leg Press", 5, 5, 250);
        workoutExerciseDAO.insertNewRecordedExercise(newExercise);

        String getQuery = "from WorkoutExercise";
        int count = entityManagerFactory.unwrap(SessionFactory.class).openSession().createQuery(getQuery).getResultList().size();

        assertEquals(3, count);
    }

    @Test
    public void testDeleteRecordedExercise() {
        workoutExerciseDAO.deleteRecordedExercise(workoutExerciseCardio);

        String getQuery = "from WorkoutExercise";
        int count = entityManagerFactory.unwrap(SessionFactory.class).openSession().createQuery(getQuery).getResultList().size();

        assertEquals(1, count);
    }

    @After
    public void tearDown() throws Exception {
        workoutExerciseDAO = null;
        em.clear();
    }
}

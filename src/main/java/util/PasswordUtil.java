package util;

import org.springframework.stereotype.Component;

import javax.crypto.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Component
public class PasswordUtil {

    private static final String ALGORITHM_NAME = "DES";
    private static final String DECRYPT_ALG_NAME = "DES/ECB/PKCS5Padding";

    private Cipher cipher;
    private SecretKey secretKey;

    public static void main(String[] args) throws Exception {
        SecretKey key = KeyGenerator.getInstance(ALGORITHM_NAME).generateKey();
        Cipher ecipher = Cipher.getInstance(ALGORITHM_NAME);
        ecipher.init(Cipher.ENCRYPT_MODE, key);
        String encrypted = encrypt("Mantas", ecipher);
        System.out.println(encrypted);
    }

    public static String encrypt(String string, Cipher ecipher) {
        byte[] stringInBytes = string.getBytes(StandardCharsets.UTF_8);
        byte[] encodedStringInBytes = new byte[0];
        try {
            encodedStringInBytes = ecipher.doFinal(Base64.getEncoder().encode(stringInBytes));
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        String encryptedString = encodedStringInBytes.toString();
        return encryptedString;
    }

    public String decrypt (String encryptedString) throws BadPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        secretKey = KeyGenerator.getInstance(ALGORITHM_NAME).generateKey();
        cipher = Cipher.getInstance(DECRYPT_ALG_NAME);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);

        byte[] encryptedStringInBytes = encryptedString.getBytes(StandardCharsets.UTF_8);
        byte[] decryptedStringInBytes = cipher.doFinal(encryptedStringInBytes);
        String decryptedString = decryptedStringInBytes.toString();
        return decryptedString;
    }

}

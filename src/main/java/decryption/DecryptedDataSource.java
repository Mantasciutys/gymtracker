package decryption;

public interface DecryptedDataSource {
    void decryptPassword();
}

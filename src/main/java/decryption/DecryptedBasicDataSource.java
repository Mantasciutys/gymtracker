package decryption;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbcp2.ConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import util.PasswordUtil;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

@Service
public class DecryptedBasicDataSource extends BasicDataSource {
    /*
    @Autowired
    private PasswordUtil passwordUtil;
    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String encryptedPassword;
    @Value("${spring.datasource.url}")
    private String url;

    @Override
    public void setPassword(String password) {
        String decryptedPassword = null;
        try {
            decryptedPassword = passwordUtil.decrypt(password);
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        super.setPassword(decryptedPassword);
    }

    @Override
    protected ConnectionFactory createConnectionFactory() throws SQLException {
        super.setDriverClassName(driverClassName);
        super.setUsername(username);
        this.setPassword(encryptedPassword);
        super.setUrl(url);
        return super.createConnectionFactory();
    }

     */
}

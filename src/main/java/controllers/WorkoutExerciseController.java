package controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import pojo.BodyPart;
import pojo.WorkoutExercise;
import service.BodyPartService;
import service.CombinationService;
import service.ExerciseService;
import service.WorkoutExerciseService;

@Controller
public class WorkoutExerciseController {
/*
	@Autowired
	private WorkoutExerciseService service;
	
	@Autowired
	private BodyPartService bodypartService;
	
	@Autowired
	private ExerciseService exerciseService;
	
	@Autowired
	private CombinationService combinationService;
	
	@GetMapping("/bodyparts/{bodypart}/{exercise}/list")
	public String getRecordedExercises(Model model, @PathVariable int bodypart, @PathVariable int exercise) {
		BodyPart bodyPart = bodypartService.getBodyPartById(bodypart);
		Exercise exer = exerciseService.getExerciseForId(exercise);
		List<WorkoutExercise> recordedExercises = service.getRecordedWorkoutsForExercise(exercise);
		
		model.addAttribute("recordedExercises", recordedExercises);
		model.addAttribute("bodyPart", bodyPart);
		model.addAttribute("exercise", exer);
		
		return "workout_exercises";
	}
	
	@PostMapping("/bodyparts/{bodypart}/{exercise}/list/add")
	public String createNewRecordedExercise(Model model, @PathVariable int bodypart, @PathVariable int exercise,
			@RequestParam("sets") int newSets, @RequestParam("reps") int newReps, @RequestParam("weights") double newWeights,
			@RequestParam("date") String newDate) {
		BodyPart bodyPart = bodypartService.getBodyPartById(bodypart);
		Exercise exer = exerciseService.getExerciseForId(exercise);
		
		Combination combination = new Combination(newSets, newReps, newWeights);
		combinationService.insertNewCombination(combination);
		
		model.addAttribute("bodyPart", bodyPart);
		model.addAttribute("exercise", exer);
		
		WorkoutExercise workoutExercise = new WorkoutExercise();
		workoutExercise.setCombination(combination);
		workoutExercise.setExercise(exer);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//sdf.setLenient(false);
		Date date = null;
		try {
			date = sdf.parse(newDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		workoutExercise.setDate(date);
		
		service.insertRecordedExercise(workoutExercise);
		
		return "redirect:/bodyparts/" + bodypart + "/" + exer.getId() + "/list";
	}
	
	@PostMapping("/bodyparts/{bodypart}/{exercise}/{workoutExercise}/delete")
	public String deleteRecordedExercise(Model model, @PathVariable int bodypart, @PathVariable int exercise,
			@PathVariable int workoutExercise) {
		
		service.deleteRecordedExercise(workoutExercise);
		return "redirect:/bodyparts/" + bodypart + "/" + exercise + "/list";
	}

 */
}

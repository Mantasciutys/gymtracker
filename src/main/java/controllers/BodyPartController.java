package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pojo.BodyPart;
import service.BodyPartService;

@Controller
public class BodyPartController {
	
	@Autowired
	private BodyPartService service;
	
	@GetMapping("/bodyparts")
	public String getBodyParts(Model model) {
		List<BodyPart> bodyParts = service.getAllBodyParts();
		model.addAttribute("bodyParts", bodyParts);
		return "bodyparts";
	}
	
	@PostMapping("/bodyparts/{bodypart}")
	public String getExercises(Model model, @PathVariable int bodypart) {
		BodyPart bodyPart = service.getBodyPartById(bodypart);
		model.addAttribute("bodypart", bodyPart);
		return "redirect:/bodyparts/{bodypart}";
	}
}

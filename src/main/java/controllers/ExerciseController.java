package controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import pojo.BodyPart;
import service.BodyPartService;
import service.ExerciseService;

@Controller
public class ExerciseController {
	
	@Autowired
	private ExerciseService service;

	@Autowired
	private BodyPartService bodyPartService;
	
//	@GetMapping("/bodyparts/{bodypart}")
//	public String getAllExercisesForBodyPart(Model model, @PathVariable(value = "bodypart") String bodyPart) {
//		List<Exercise> exercises = service.getExercisesForBodypart(bodyPart);
//		model.addAttribute("bodyPart", bodyPart);
//		model.addAttribute("allExercises", exercises);
//		return "exercises";
//	}
/*
	@GetMapping("/bodyparts/{bodypart}")
	public String getAllExercisesForBodyPart(Model model, @PathVariable(value = "bodypart") int bodyPart) {
		BodyPart bodypart = bodyPartService.getBodyPartById(bodyPart);
		List<Exercise> exercises = service.getExercisesForBodyPart(bodyPart);
		model.addAttribute("bodypart", bodypart);
		model.addAttribute("allExercises", exercises);
		return "exercises";
	}
	
//	@GetMapping("delete/{bodypart}/{exercise}")
//	public String deleteExercise(@PathVariable String bodypart, @PathVariable String exercise) {
//		service.deleteExercise(exercise);
//		return "redirect:/bodyparts/" + bodypart;
//	}

	@GetMapping("delete/{bodypart}/{exercise}")
	public String deleteExercise(@PathVariable int bodypart, @PathVariable String exercise) {
		service.deleteExercise(exercise);
		return "redirect:/bodyparts/" + bodypart;
	}
	
	@PostMapping("/bodyparts/{bodypart}/addExercise")
	public String saveNewExercise(@PathVariable int bodypart,
			@RequestParam("newExercise") String newExercise) {
		service.addNewExercise(newExercise, bodypart);
		return "redirect:/bodyparts/" + bodypart;
	}

 */
}

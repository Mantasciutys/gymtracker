package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.BodyPartDAO;
import pojo.BodyPart;

@Service
public class BodyPartService {
	
	@Autowired
	private BodyPartDAO bodyPartDao;
	
	public List<BodyPart> getAllBodyParts() {
		return bodyPartDao.getAllBodyParts();
	}
	
	public BodyPart getBodyPart(String bodypart) {
		return bodyPartDao.getRequiredBodyPart(bodypart);
	}

	public BodyPart getBodyPartById(int id) {
		return null;//bodyPartDao.getBodyPart(id);
	}
}

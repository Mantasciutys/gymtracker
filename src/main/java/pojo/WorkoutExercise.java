package pojo;

import javax.persistence.*;
import java.util.Date;

@Entity
public class WorkoutExercise {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@OneToOne(targetEntity = GeneralExercise.class, cascade = CascadeType.ALL)
	private GeneralExercise exercise;
	private Date date;
	private String user;

	public WorkoutExercise(GeneralExercise exercise, Date date, String user) {
		this.exercise = exercise;
		this.date = date;
		this.user = user;
	}

	public WorkoutExercise() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public GeneralExercise getExercise() {
		return exercise;
	}

	public void setExercise(GeneralExercise exercise) {
		this.exercise = exercise;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
}

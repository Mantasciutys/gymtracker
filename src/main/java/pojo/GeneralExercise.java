package pojo;

import javax.persistence.*;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class GeneralExercise {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    //private List<BodyPart> activeBodyParts;


    public GeneralExercise() {
    }

    public GeneralExercise(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public List<BodyPart> getActiveBodyParts() {
//        return activeBodyParts;
//    }
//
//    public void setActiveBodyParts(List<BodyPart> activeBodyParts) {
//        this.activeBodyParts = activeBodyParts;
//    }
}


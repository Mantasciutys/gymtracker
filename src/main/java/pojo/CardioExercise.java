package pojo;

import javax.persistence.Entity;

@Entity
public class CardioExercise extends GeneralExercise {
    private double time;

    public CardioExercise() {
        super();
    }

    public CardioExercise(String name, double time) {
        super(name);
        this.time = time;
    }
}

package dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import pojo.GeneralExercise;
import pojo.WorkoutExercise;

@Repository
public class WorkoutExerciseDAO {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    private SessionFactory getSessionFactory() {
        return entityManagerFactory.unwrap(SessionFactory.class);
    }

    public List<WorkoutExercise> getAllRecordedWorkouts() {
        String getQuery = "from WorkoutExercise";
        Query query = getSessionFactory().openSession().createQuery(getQuery);
        return query.getResultList();
    }

    public List<WorkoutExercise> getAllRecordedWorkoutsWithExercise(GeneralExercise exercise) {
        String getQuery = "from WorkoutExercise where exercise = :exercise";
        Query query = getSessionFactory().openSession().createQuery(getQuery);
        query.setParameter("exercise", exercise);
        return query.getResultList();
    }

    public void insertNewRecordedExercise(GeneralExercise exercise) {
        WorkoutExercise workoutExercise = new WorkoutExercise(exercise, new Date(), "default");
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.persist(workoutExercise);
        em.getTransaction().commit();
    }

    public void deleteRecordedExercise(WorkoutExercise recordedExercise) {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();

        WorkoutExercise foundExercise = em.find(WorkoutExercise.class, recordedExercise.getId());

        if (foundExercise != null) {
            em.remove(foundExercise);
        }

        em.getTransaction().commit();
    }
}

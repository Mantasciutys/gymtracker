package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import pojo.BodyPart;

@Repository
public class BodyPartDAO {

	@Autowired
    private EntityManagerFactory entityManagerFactory;

    private SessionFactory getSessionFactory() {
        return entityManagerFactory.unwrap(SessionFactory.class);
    }
	
	public List<BodyPart> getAllBodyParts() {
    	String getQuery = "from BodyPart";
		Query query = getSessionFactory().openSession().createQuery(getQuery);
		return query.getResultList();
	}
	
	public BodyPart getRequiredBodyPart(String bodyPart) {
    	String getQuery = "from BodyPart where name = :bodyPart";
		Query query = getSessionFactory().openSession().createQuery(getQuery);
		query.setParameter("bodyPart", bodyPart);
		List<BodyPart> bodyParts = query.getResultList();

		if (!bodyParts.isEmpty()) {
			return bodyParts.get(0);
		}
		return null;
	}

	public void insertBodyPart(BodyPart bodyPart) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.persist(bodyPart);
		entityManager.getTransaction().commit();
	}

}
